/**
 * Created by ksheppard on 22/04/2016.
 */
(function($) {
    $.fn.escapeHtml = function() {
        return this.each(function(idx, elm) {
            var cleanText = $(elm).html()
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;");
            $(elm).html(cleanText);
        });
    }

})(jQuery);
